import client.HttpStyxClient;
import client.StyxClient;
import client.Whence;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class StyxClientTest {

    private final static String LOG_NAME = "test-log";

    StyxClient client = new HttpStyxClient();

    @BeforeEach
    void beforeEach() throws Exception {
        try {
            client.deleteLog(LOG_NAME);
        } catch (Exception ex) {
            // ignore log doesn't exists error
        }
        client.createLog(LOG_NAME);
    }

    @AfterEach
    void afterEach() throws Exception {
        client.close();
    }

    @Test
    void writeReadTest() throws IOException, InterruptedException {
        var events = genRecords(1_000);
        for (var event : events) {
            client.write(LOG_NAME, event.getBytes());
        }

        var readEvents = new ArrayList<String>();
        for (int i = 0; i < events.size(); i++) {
            var event = client.read(LOG_NAME, Whence.ORIGIN, i, 1);
            readEvents.add(new String(event));
        }

        assertThat(readEvents).isEqualTo(events);
    }

    private List<String> genRecords(int count) {
        var events = new ArrayList<String>();
        for (int i = 0; i < count; i++) {
            events.add(UUID.randomUUID().toString());
        }
        return events;
    }

}
