package client;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class WsStyxClient implements StyxClient {

    private final static String URL = "http://127.0.0.1:8000";

    private final HttpClient http = HttpClient.newHttpClient();


    @Override
    public void createLog(String logName) throws IOException, InterruptedException {
        var request = HttpRequest.newBuilder()
            .uri(URI.create(URL + "/logs"))
            .POST(HttpRequest.BodyPublishers.ofString("name=" + logName))
            .header("Content-Type", "application/x-www-form-urlencoded")
            .build();

        var response = http.send(request, HttpResponse.BodyHandlers.ofString());
        if (response.statusCode() != 200) {
            throw new IOException("http-status=" + response.statusCode() + "|body=" + response.body());
        }
    }

    @Override
    public void deleteLog(String logName) throws IOException, InterruptedException {
        var request = HttpRequest.newBuilder()
            .uri(URI.create(URL + "/logs/" + logName))
            .DELETE()
            .build();

        var response = http.send(request, HttpResponse.BodyHandlers.ofString());
        if (response.statusCode() != 200) {
            throw new IOException("http-status=" + response.statusCode() + "|body=" + response.body());
        }
    }

    @Override
    public byte[] read(String logName, Whence whence, int position, int count) throws IOException, InterruptedException {
        whence = whence == null ? Whence.ORIGIN : whence;
        var query = "whence=" + whence.name().toLowerCase() + "&position=" + position + "&count=" + count;
        var request = HttpRequest.newBuilder()
            .uri(URI.create(URL + "/logs/" + logName + "/records?" + query))
            .build();

        var response = http.send(request, HttpResponse.BodyHandlers.ofByteArray());
        if (response.statusCode() != 200) {
            throw new IOException("http-status=" + response.statusCode() + "|body=" + response.body());
        }

        return response.body();
    }

    @Override
    public void write(String logName, byte[] data) throws IOException, InterruptedException {
        var request = HttpRequest.newBuilder()
            .uri(URI.create(URL + "/logs/" + logName + "/records"))
            .POST(HttpRequest.BodyPublishers.ofByteArray(data))
            .build();

        var response = http.send(request, HttpResponse.BodyHandlers.ofString());
        if (response.statusCode() != 200) {
            throw new IOException("http-status=" + response.statusCode() + "|body=" + response.body());
        }
    }


    @Override
    public void close() throws IOException {
    }

}
