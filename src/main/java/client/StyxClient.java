package client;

import java.io.Closeable;
import java.io.IOException;

public interface StyxClient extends Closeable {

    byte[] read(String logName, Whence origin, int position, int count) throws IOException, InterruptedException;

    void write(String logName, byte[] data) throws IOException, InterruptedException;

    void deleteLog(String logName) throws IOException, InterruptedException;

    void createLog(String logName) throws IOException, InterruptedException;
}
