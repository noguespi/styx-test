package client;

public enum Whence {
    ORIGIN,
    START,
    END;
}
